package app.music.play.Models;

import java.io.Serializable;

public class AudioModel implements Serializable {
    private String aPath;
    private String aName;
    private String aArtist;
    private String duration;

    public AudioModel(String aPath, String aName, String aArtist, String duration) {
        this.aPath = aPath;
        this.aName = aName;
        this.aArtist = aArtist;
        this.duration = duration;
    }

    public String getaPath() {
        return aPath;
    }

    public void setaPath(String aPath) {
        this.aPath = aPath;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String getaArtist() {
        return aArtist;
    }

    public void setaArtist(String aArtist) {
        this.aArtist = aArtist;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
