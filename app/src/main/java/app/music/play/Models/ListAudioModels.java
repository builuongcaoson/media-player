package app.music.play.Models;

import java.io.Serializable;
import java.util.List;

public class ListAudioModels implements Serializable {
    private List<AudioModel> audioModelList;

    public ListAudioModels(List<AudioModel> audioModelList) {
        this.audioModelList = audioModelList;
    }

    public List<AudioModel> getAudioModelList() {
        return audioModelList;
    }

    public void setAudioModelList(List<AudioModel> audioModelList) {
        this.audioModelList = audioModelList;
    }
}
