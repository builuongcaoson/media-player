package app.music.play.InterfaceCallBack;

import java.util.List;

import app.music.play.Models.AudioModel;

public interface onClickCallBack {
    void onClickMusic(List<AudioModel> audioModel, int position);
}
