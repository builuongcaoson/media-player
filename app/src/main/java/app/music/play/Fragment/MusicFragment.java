package app.music.play.Fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.music.play.Adapter.AdapterMusic;
import app.music.play.Constants;
import app.music.play.Models.ListAudioModels;
import app.music.play.onClickCallBackFragment;
import app.music.play.Models.AudioModel;
import app.music.play.InterfaceCallBack.onClickCallBack;
import app.music.play.Service.NotificationService;
import app.music.play.R;

public class MusicFragment extends Fragment implements onClickCallBack {

    private static final int MY_PERMISTION = 123;

    private onClickCallBackFragment callBackFragment;
    private RecyclerView recyclerView;
    private AdapterMusic adapterMusic;

    private ListAudioModels listAudioModels;

    public MusicFragment() {
        // Required empty public constructor
    }

    public MusicFragment(onClickCallBackFragment callBackFragment) {
        this.callBackFragment = callBackFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewMusic);
        instance();
        if (checkPermistion()) {
            adapterMusic = new AdapterMusic(getContext(), getAllAudioFromDevice(getContext()), this);
            recyclerView.setAdapter(adapterMusic);
        }
        return view;
    }

    private boolean checkPermistion() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISTION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISTION);
            }
            return false;
        }
        return true;
    }

    public List<AudioModel> getAllAudioFromDevice(final Context context) {
        List<AudioModel> list = new ArrayList<>();
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);
        int count;
        if (cur != null) {
            count = cur.getCount();
            if (count > 0) {
                while (cur.moveToNext()) {
                    String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String title = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artist = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String duration = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION));
                    AudioModel audioModel = new AudioModel(data, title, artist, duration);
                    list.add(audioModel);
                }
            }
        }
        cur.close();
        return list;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISTION:
                checkPermistionResult(grantResults);
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkPermistionResult(int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permistion Grant", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getContext(), "Permistion No Grant", Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    public void instance() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //Tạo vạch ngăn cách giữa các item
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), ((LinearLayoutManager) manager).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }


    private void sendAudioModel(List<AudioModel> audioModel, int position) {
        this.callBackFragment.onClick(audioModel, position);
    }

    @Override
    public void onClickMusic(List<AudioModel> audioModel, int position) {
        this.listAudioModels = new ListAudioModels(audioModel);

        sendAudioModel(audioModel, position);
        Intent serviceIntent = new Intent(getContext(), NotificationService.class);
        serviceIntent.putExtra("listAudio", this.listAudioModels);
        serviceIntent.putExtra("position", position);
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        getContext().startService(serviceIntent);
    }
}
