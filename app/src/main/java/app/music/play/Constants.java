package app.music.play;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;

public class Constants extends Application {

    public interface ACTION {
        public static String MAIN_ACTION = "com.example.musicnotification.main";
        public static String INIT_ACTION = "com.example.musicnotification.init";
        public static String PREV_ACTION = "com.example.musicnotification.prev";
        public static String PLAY_ACTION = "com.example.musicnotification.play";
        public static String NEXT_ACTION = "com.example.musicnotification.next";
        public static String STARTFOREGROUND_ACTION = "com.example.musicnotification.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.example.musicnotification.stopforeground";
    }
    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }

    public static Bitmap getImageMusic(String data) {
        MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
        byte[] art;
        Bitmap bitmap = null;
        metaRetriver.setDataSource(data);
        try {
            art = metaRetriver.getEmbeddedPicture();
            bitmap = BitmapFactory
                    .decodeByteArray(art, 0, art.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
