package app.music.play;

import java.util.List;

import app.music.play.Models.AudioModel;

public interface onClickCallBackFragment {
     void onClick(List<AudioModel> audioModel, int position);
}
