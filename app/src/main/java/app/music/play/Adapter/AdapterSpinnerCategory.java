package app.music.play.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import app.music.play.R;

public class AdapterSpinnerCategory extends BaseAdapter {

    private Context context;
    private String[] categoryList;

    public AdapterSpinnerCategory(Context context, String[] categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false);
        TextView names = view.findViewById(R.id.textViewCategorySpinner);
        names.setText(categoryList[position]);
        return view;
    }
}
