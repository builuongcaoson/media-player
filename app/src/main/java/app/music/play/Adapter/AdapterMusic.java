package app.music.play.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.music.play.Models.AudioModel;
import app.music.play.InterfaceCallBack.onClickCallBack;
import app.music.play.R;

public class AdapterMusic extends RecyclerView.Adapter<AdapterMusic.ViewHolder> {

    private Context context;
    private List<AudioModel> audioModelList;

    private onClickCallBack callBack;
    private MediaMetadataRetriever metaRetriver;
    private byte[] art;

    public AdapterMusic(Context context, List<AudioModel> audioModelList, onClickCallBack clickCallBack) {
        this.context = context;
        this.audioModelList = audioModelList;
        this.callBack = clickCallBack;
        metaRetriver = new MediaMetadataRetriever();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item_music, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final AudioModel audioModel = audioModelList.get(position);
        holder.imageView.setImageBitmap(getImageMusic(audioModel.getaPath()));
        holder.textViewTitle.setText(audioModel.getaName());
        holder.textViewArtist.setText(audioModel.getaArtist());
        holder.textViewDuration.setText(milliSecondsToTimer(audioModel.getDuration()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onClickMusic(audioModelList, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return audioModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textViewTitle, textViewArtist, textViewDuration;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageViewMusic);
            textViewTitle = itemView.findViewById(R.id.textViewNameMusic);
            textViewArtist = itemView.findViewById(R.id.textViewArtist);
            textViewDuration = itemView.findViewById(R.id.textViewDuration);
        }
    }

    private Bitmap getImageMusic(String data) {
        Bitmap bitmap = null;
        metaRetriver.setDataSource(data);
        try {
            art = metaRetriver.getEmbeddedPicture();
            bitmap = BitmapFactory
                    .decodeByteArray(art, 0, art.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private String milliSecondsToTimer(String duration) {
        String finalTimerString = "";
        String secondsString;
        int hours = (int) (Long.parseLong(duration) / (1000 * 60 * 60));
        int minutes = (int) (Long.parseLong(duration) % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((Long.parseLong(duration) % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        finalTimerString = finalTimerString + minutes + ":" + secondsString;
        return finalTimerString;
    }
}
