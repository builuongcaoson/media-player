package app.music.play.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Random;

import app.music.play.Constants;
import app.music.play.Activity.MainActivity;

import app.music.play.Models.ListAudioModels;
import app.music.play.R;

public class NotificationService extends Service implements MediaPlayer.OnCompletionListener {

    private Notification status;
    private MediaPlayer mediaPlayer;

    private ListAudioModels listAudioModels;
    private int position;

    private String pathMusic;
    private String artist;
    private String title;
    private boolean isCheckedPlay, isRepeatAll, temp, isCheckRandomMusic;
    private int duration;

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case Constants.ACTION.STARTFOREGROUND_ACTION:
                this.listAudioModels = (ListAudioModels) intent.getSerializableExtra("listAudio");
                if (this.listAudioModels != null) {
                    this.position = intent.getIntExtra("position", 0);
                    this.pathMusic = this.listAudioModels.getAudioModelList().get(this.position).getaPath();
                    this.title = this.listAudioModels.getAudioModelList().get(this.position).getaName();
                    this.artist = this.listAudioModels.getAudioModelList().get(this.position).getaArtist();
                    startService(this.pathMusic, this.title, this.artist);
                    runMedia();
                }
                break;
            case Constants.ACTION.PREV_ACTION:
                prevMusic();
                break;
            case Constants.ACTION.PLAY_ACTION:
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                } else mediaPlayer.start();
                this.isCheckedPlay = !isCheckedPlay;
                updateNotification(this.pathMusic, this.title, this.artist, this.isCheckedPlay);
                sendBroadCastToActivity(0, isCheckedPlay);
                break;
            case Constants.ACTION.NEXT_ACTION:
                nextMusic();
                break;
            case Constants.ACTION.STOPFOREGROUND_ACTION:
                mediaPlayer.stop();
                mediaPlayer.release();
                isCheckedPlay = false;
                stopForeground(true);
                stopSelf();
                sendBroadCastToActivity(1, isCheckedPlay);
                break;
        }
        return START_STICKY;
    }

    private void prevMusic() {
        if (this.position == 0) {
            this.position = this.listAudioModels.getAudioModelList().size() - 1;
        } else this.position--;
        changeNextOrPrevMusic();
    }

    private void nextMusic() {
        if (this.position == this.listAudioModels.getAudioModelList().size() - 1) {
            this.position = 0;
        } else this.position++;
        changeNextOrPrevMusic();
    }

    private void changeNextOrPrevMusic() {
        this.pathMusic = this.listAudioModels.getAudioModelList().get(this.position).getaPath();
        this.title = this.listAudioModels.getAudioModelList().get(this.position).getaName();
        this.artist = this.listAudioModels.getAudioModelList().get(this.position).getaArtist();

        runMedia();
        changeView();
        updateMusicActivity();
    }

    private void updateMusicActivity() {
        Intent intent = new Intent();
        intent.setAction("SOS");
        intent.putExtra("key", 2);
        intent.putExtra("position", this.position);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void changeView() {
        this.isCheckedPlay = false;
        updateNotification(this.pathMusic, this.title, this.artist, false);
        this.mediaPlayer.start();
    }

    private void runMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(this.pathMusic));
        mediaPlayer.setLooping(false);
        mediaPlayer.start();
    }

    private void sendBroadCastToActivity(int key, boolean isCheckedPlay) {
        Intent intent = new Intent();
        intent.setAction("SOS");
        intent.putExtra("key", key);
        if (key == 0) {
            intent.putExtra("isPlay", isCheckedPlay);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private Notification getNotification(String path, String title, String artist, boolean isCheckedPlay) {
        runMedia();

        RemoteViews views = new RemoteViews(getPackageName(), R.layout.status_bar);
        RemoteViews bigViews = new RemoteViews(getPackageName(), R.layout.status_bar_expanded);
        views.setImageViewBitmap(R.id.status_bar_icon, getImageMusic(path));
        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        bigViews.setImageViewBitmap(R.id.status_bar_album_art, Constants.getImageMusic(path));
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Intent previousIntent = new Intent(this, NotificationService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, previousIntent, 0);
        Intent playIntent = new Intent(this, NotificationService.class);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0, playIntent, 0);
        Intent nextIntent = new Intent(this, NotificationService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0, nextIntent, 0);
        Intent closeIntent = new Intent(this, NotificationService.class);
        closeIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);
        views.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);
        views.setOnClickPendingIntent(R.id.status_bar_next, pnextIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_next, pnextIntent);
        views.setOnClickPendingIntent(R.id.status_bar_prev, ppreviousIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_prev, ppreviousIntent);
        views.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);

        if (isCheckedPlay) {
            views.setImageViewResource(R.id.status_bar_play, R.drawable.ic_play_arrow_black_24dp);
            bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.ic_play_arrow_black_big);
        } else {
            views.setImageViewResource(R.id.status_bar_play, R.drawable.ic_pause_black_24dp);
            bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.ic_pause_black_big);
        }

        views.setTextViewText(R.id.status_bar_track_name, title);
        bigViews.setTextViewText(R.id.status_bar_track_name, title);
        views.setTextViewText(R.id.status_bar_artist_name, artist);
        bigViews.setTextViewText(R.id.status_bar_artist_name, artist);

        status = new Notification.Builder(this).build();
        status.contentView = views;
        status.bigContentView = bigViews;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        //Icon status bar
        status.icon = R.drawable.ic_audiobook;
        status.contentIntent = pendingIntent;
        return status;
    }

    private void startService(String path, String title, String artist) {
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, getNotification(path, title, artist, isCheckedPlay));
    }

    public static Bitmap getImageMusic(String data) {
        MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
        byte[] art;
        Bitmap bitmap = null;
        metaRetriver.setDataSource(data);
        try {
            art = metaRetriver.getEmbeddedPicture();
            bitmap = BitmapFactory
                    .decodeByteArray(art, 0, art.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private void updateNotification(String path, String title, String artist, boolean isCheckedPlay) {
        Notification notification = getNotification(path, title, artist, isCheckedPlay);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);

        playOrPauseMusic(isCheckedPlay);
    }

    private void playOrPauseMusic(boolean isCheckedPlay) {
        if (isCheckedPlay) {
            mediaPlayer.pause();
        } else mediaPlayer.start();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int i = intent.getIntExtra("key", 0);
            boolean temp = intent.getBooleanExtra("toSeek", false);
            if (temp) {
                setSeekMediaPlayer(i);
                return;
            } else if (intent.getIntExtra("position", -1) != -1) {
                position = intent.getIntExtra("position", 0);

                pathMusic = listAudioModels.getAudioModelList().get(position).getaPath();
                title = listAudioModels.getAudioModelList().get(position).getaName();
                artist = listAudioModels.getAudioModelList().get(position).getaArtist();

                isCheckedPlay = !isCheckedPlay;
                updateNotification(pathMusic, title, artist, isCheckedPlay);
                runMedia();
                return;
            }
            switch (i) {
                case 0:
                    break;
                case 1:
                    changePlayOrPauseMusic(1);
                    break;
                case 2:
                    changePlayOrPauseMusic(2);
                    break;
                case 5:
                    isRepeatAll = false;
                    mediaPlayer.setLooping(true);
                    break;
                case 4:
                    isRepeatAll = true;
                    mediaPlayer.setLooping(false);
                    mediaPlayer.setOnCompletionListener(NotificationService.this);
                    break;
                case 6:
                    isRepeatAll = false;
                    mediaPlayer.setLooping(false);
                    mediaPlayer.setOnCompletionListener(NotificationService.this);
                    break;
                case 7:
                    isCheckRandomMusic = true;
                    mediaPlayer.setOnCompletionListener(NotificationService.this);
                    break;
                case 8:
                    isCheckRandomMusic = false;
                    mediaPlayer.setOnCompletionListener(NotificationService.this);
                    break;
            }
        }
    };

    private void setSeekMediaPlayer(int i) {
        this.mediaPlayer.seekTo(i * 1000);
    }

    private void changePlayOrPauseMusic(int i) {
        if (i == 1) {
            isCheckedPlay = false;
            updateNotification(pathMusic, title, artist, false);
            mediaPlayer.start();
        } else {
            isCheckedPlay = true;
            updateNotification(pathMusic, title, artist, true);
            mediaPlayer.pause();
        }
    }

    @Override
    public void onCreate() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter("NOW"));
        super.onCreate();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (isCheckRandomMusic){
            this.position = new Random().nextInt(((this.listAudioModels.getAudioModelList().size() - 1) - 0 + 1)) + 0;
        }else {
            if (position == listAudioModels.getAudioModelList().size() - 1) {
                if (!isRepeatAll) {
                    temp = true;
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    isCheckedPlay = false;
                    stopForeground(true);
                    stopSelf();
                    sendBroadCastToActivity(1, isCheckedPlay);
                } else position = 0;
            } else position++;
        }

        if (!temp) {
            changeNextOrPrevMusic();

            mediaPlayer.setOnCompletionListener(this);

            temp = false;
        }
    }
}
