package app.music.play.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

import app.music.play.Constants;
import app.music.play.Fragment.AlbumFragment;
import app.music.play.Fragment.ArtistFragment;
import app.music.play.Fragment.FolderFragment;
import app.music.play.Fragment.ListMusicFragment;
import app.music.play.Fragment.MusicFragment;
import app.music.play.InterfaceCallBack.onClickCallBack;
import app.music.play.Models.AudioModel;
import app.music.play.Models.ListAudioModels;
import app.music.play.R;
import app.music.play.Service.NotificationService;
import app.music.play.onClickCallBackFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, onClickCallBack, onClickCallBackFragment, View.OnClickListener {

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private BottomNavigationView bottomNavigationView;
    private RelativeLayout recyclerViewMusicPlay;
    private TextView textViewTitleMusic, textViewArtistMusic;
    private ImageView imageViewListMusic, imageViewPlayMusic;

    private ListMusicFragment listMusicFragment;
    private MusicFragment musicFragment;
    private ArtistFragment artistFragment;
    private AlbumFragment albumFragment;
    private FolderFragment folderFragment;

    private AudioModel audioModel;
    private ListAudioModels audioModelList;

    private boolean isCheckedPause;
    private boolean isDelete;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        initFragment();
        initializationToogleAndToolBar();
        bottomNavigationView.setSelectedItemId(R.id.nav_music);
        setFragment(musicFragment);

        imageViewPlayMusic.setOnClickListener(this);
        imageViewListMusic.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_list_music:
                        setFragment(listMusicFragment);
                        return true;
                    case R.id.nav_music:
                        setFragment(musicFragment);
                        return true;
                    case R.id.nav_artist:
                        setFragment(artistFragment);
                        return true;
                    case R.id.nav_album:
                        setFragment(albumFragment);
                        return true;
                    case R.id.nav_folder:
                        setFragment(folderFragment);
                        return true;
                }
                return false;
            }
        });
    }

    private void initFragment() {
        listMusicFragment = new ListMusicFragment();
        musicFragment = new MusicFragment(this);
        artistFragment = new ArtistFragment();
        albumFragment = new AlbumFragment();
        folderFragment = new FolderFragment();
    }

    private void anhXa() {
        drawerLayout = findViewById(R.id.drawerLayoutMain);
        toolbar = findViewById(R.id.toolBarMain);
        navigationView = findViewById(R.id.navViewMain);
        bottomNavigationView = this.findViewById(R.id.navViewBottom);

        recyclerViewMusicPlay = this.findViewById(R.id.recyclerViewMusicPlay);
        textViewArtistMusic = this.findViewById(R.id.textViewArtistMusic);
        textViewTitleMusic = this.findViewById(R.id.textViewTitleMusic);
        imageViewListMusic = this.findViewById(R.id.imageViewListMusic);
        imageViewPlayMusic = this.findViewById(R.id.imageViewPlayMusic);
    }

    private void initializationToogleAndToolBar() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white);
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.linearLayoutChange, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openOrCloseNav();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
        super.onConfigurationChanged(newConfig);
    }

    @SuppressLint("WrongConstant")
    private void openOrCloseNav() {
        if (!drawerLayout.isDrawerOpen(Gravity.START)) drawerLayout.openDrawer(Gravity.START);
        else drawerLayout.closeDrawer(Gravity.END);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_audioBook:
                Toast.makeText(this, "Audio Book", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_cutMusic:
                Toast.makeText(this, "Cắt nhạc", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_catogary:
                Toast.makeText(this, "Thể loại", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_shareNear:
                Toast.makeText(this, "Chia sẻ file", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_theme:
                Toast.makeText(this, "Chủ đề", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_stopTime:
                Toast.makeText(this, "Hẹn giờ", Toast.LENGTH_SHORT).show();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewListMusic:
                Toast.makeText(this, "ListMusic", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageViewPlayMusic:
                changePlayOrPause();
                break;
        }
    }

    private void changePlayOrPause() {
        isCheckedPause = !isCheckedPause;
        if (isCheckedPause) {
            imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
            sendBroadcastService(2);
        } else {
            if (isDelete) {
                Intent serviceIntent = new Intent(this, NotificationService.class);
                serviceIntent.putExtra("listAudio", this.audioModelList);
                serviceIntent.putExtra("position", position);
                serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                startService(serviceIntent);
                imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
                isDelete = false;
            } else {
                imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
                sendBroadcastService(1);
            }
        }
    }

    private void sendBroadcastService(int i) {
        Intent intent = new Intent();
        intent.setAction("NOW");
        intent.putExtra("key", i);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int i = intent.getIntExtra("key", 0);
            switch (i) {
                case 1:
                    isCheckedPause = true;
                    isDelete = true;
                    imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
                    break;
                case 0:
                    isCheckedPause = intent.getBooleanExtra("isPlay", false);
                    if (isCheckedPause) {
                        imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
                    } else imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
                    break;
                case 2:
                    position = intent.getIntExtra("position", 0);
                    textViewTitleMusic.setText(audioModelList.getAudioModelList().get(position).getaName());
                    textViewArtistMusic.setText(audioModelList.getAudioModelList().get(position).getaArtist());
                    break;
            }
        }
    };

    @Override
    protected void onStart() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter("SOS"));
        super.onStart();
    }

    @Override
    public void onClickMusic(List<AudioModel> audioModel, int position) {

    }

    @Override
    public void onClick(List<AudioModel> audioModel, int position) {
        this.audioModel = audioModel.get(position);
        this.audioModelList = new ListAudioModels(audioModel);
        this.position = position;

        recyclerViewMusicPlay.setVisibility(View.VISIBLE);
        textViewTitleMusic.setText(this.audioModel.getaName());
        textViewArtistMusic.setText(this.audioModel.getaArtist());

        Intent intent = new Intent(this, PlayMusicActivity.class);
        intent.putExtra("listAudio", this.audioModelList);
        intent.putExtra("position", this.position);
        startActivity(intent);
    }
}
