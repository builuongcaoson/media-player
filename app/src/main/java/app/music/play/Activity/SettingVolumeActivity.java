package app.music.play.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Virtualizer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import app.music.play.Adapter.AdapterSpinnerCategory;
import app.music.play.R;
import app.music.play.View.CircleSeekBar.Croller;
import app.music.play.View.CircleSeekBar.OnCrollerChangeListener;

public class SettingVolumeActivity extends AppCompatActivity {

    private AudioManager audioManager;
    private BassBoost bassBoost;
    private Virtualizer virtualizer;

    private Spinner spinner;
    private AdapterSpinnerCategory adapter;
    private Toolbar toolBarSettingVolume;
    private Croller seekBarVolume, seekBarBass, seekBarVirtualizer;

    private String[] categoryList = {"Tùy chỉnh", "Cổ điển", "Dance", "Flat", "Folk", "Heavy", "Metal"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_volume);
        anhXa();
        initToolBar();
        initAudio();
        adapter = new AdapterSpinnerCategory(this, categoryList);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initAudio() {
        try {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            seekBarVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
            seekBarVolume.setOnProgressChangedListener(new Croller.onProgressChangedListener() {
                @Override
                public void onProgressChanged(int progress) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
            bassBoost = new BassBoost(0, new MediaPlayer().getAudioSessionId());
            bassBoost.setEnabled(true);
            seekBarBass.setProgress(bassBoost.getRoundedStrength() / 10);
            seekBarBass.setOnCrollerChangeListener(new OnCrollerChangeListener() {
                @Override
                public void onProgressChanged(Croller croller, int progress) {
                    if (bassBoost.getStrengthSupported()) {
                        bassBoost.setStrength((short) (progress * 10));
                    }
                }

                @Override
                public void onStartTrackingTouch(Croller croller) {

                }

                @Override
                public void onStopTrackingTouch(Croller croller) {

                }
            });
            virtualizer = new Virtualizer(0, new MediaPlayer().getAudioSessionId());
            virtualizer.setEnabled(true);
            seekBarVirtualizer.setProgress(virtualizer.getRoundedStrength() / 10);
            seekBarVirtualizer.setOnCrollerChangeListener(new OnCrollerChangeListener() {
                @Override
                public void onProgressChanged(Croller croller, int progress) {
                    if (virtualizer.getStrengthSupported()) {
                        virtualizer.setStrength((short) (progress * 10));
                    }
                }

                @Override
                public void onStartTrackingTouch(Croller croller) {

                }

                @Override
                public void onStopTrackingTouch(Croller croller) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initToolBar() {
        setSupportActionBar(toolBarSettingVolume);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void anhXa() {
        this.spinner = findViewById(R.id.spinnerSettingVolume);
        this.toolBarSettingVolume = findViewById(R.id.toolBarSettingVolume);
        this.seekBarVolume = findViewById(R.id.seekBarVolume);
        this.seekBarBass = findViewById(R.id.seekBarBass);
        this.seekBarVirtualizer = findViewById(R.id.seekBarVirtualizer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting_music, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuBalance:
                Toast.makeText(this, "Balance", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
