package app.music.play.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import app.music.play.Constants;
import app.music.play.Models.ListAudioModels;
import app.music.play.R;
import app.music.play.Service.NotificationService;

public class PlayMusicActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView imageViewImageMusic, imageViewPlayMusic, imageViewRepeatMusic, imageViewPrevMusic, imageViewNextMusic;
    private TextView textViewTitleMusic, textViewArtistMusic, textViewCurrentTime, textViewTimeMusic;
    private ImageView imageViewRandomMusic;
    private SeekBar seekbarPlayMusic;

    private final Handler handler = new Handler();

    private String path;
    private String title;
    private String artist;
    private String time;

    private ListAudioModels listAudioModels;
    private int position;
    private int positionRandom;

    private int secondTotal;
    private int secondSeekBar = 0;
    private boolean isCheckedPause;
    private boolean isDelete;
    private boolean isChangeSeekBar;

    private boolean isChangeMusic;
    private int countRepeat;
    private int countRandom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        anhXa();
        getAndSetData();
        initToolBar();
        initValue();

        seekbarPlayMusic.setMax(secondTotal);
        PlayMusicActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isCheckedPause) {
                    seekbarPlayMusic.setProgress(seekbarPlayMusic.getProgress());
                    textViewCurrentTime.setText(formatTime(seekbarPlayMusic.getProgress()));
                } else if (secondSeekBar == secondTotal) {
                    if (countRepeat == 2) {
                        repeatMusic();
                    }
                } else {
                    seekbarPlayMusic.setProgress((secondSeekBar++));
                    textViewCurrentTime.setText(formatTime(seekbarPlayMusic.getProgress()));
                }
                handler.postDelayed(this, 1000);
            }
        });

        seekbarPlayMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sendBroadcastService(seekBar.getProgress(), !isChangeSeekBar, false);
                secondSeekBar = seekBar.getProgress();
            }
        });

        imageViewRepeatMusic.setOnClickListener(this);
        imageViewPlayMusic.setOnClickListener(this);
        imageViewPrevMusic.setOnClickListener(this);
        imageViewNextMusic.setOnClickListener(this);
        imageViewRandomMusic.setOnClickListener(this);
    }

    private void sendBroadcastService(int i, boolean isSeekTo, boolean isChangeMusic) {
        Intent intent = new Intent();
        intent.setAction("NOW");
        intent.putExtra("key", i);
        if (isSeekTo) {
            intent.putExtra("toSeek", true);
        }
        if (isChangeMusic) {
            intent.putExtra("position", this.position);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private String formatTime(int progress) {
        int minutes = (progress % 3600) / 60;
        int seconds = progress % 60;
        return String.format("%02d:%02d", minutes, seconds);
    }

    private void initValue() {
        String time = textViewTimeMusic.getText().toString();
        try {
            Date date = new SimpleDateFormat("mm:ss").parse(time);
            secondTotal = date.getMinutes() * 60 + date.getSeconds();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void getAndSetData() {
        Intent intent = getIntent();
        this.listAudioModels = (ListAudioModels) intent.getSerializableExtra("listAudio");
        this.position = intent.getIntExtra("position", 0);

        path = this.listAudioModels.getAudioModelList().get(this.position).getaPath();
        title = this.listAudioModels.getAudioModelList().get(this.position).getaName();
        artist = this.listAudioModels.getAudioModelList().get(this.position).getaArtist();
        time = this.listAudioModels.getAudioModelList().get(this.position).getDuration();

        imageViewImageMusic.setImageBitmap(Constants.getImageMusic(path));
        textViewTitleMusic.setText(title);
        textViewArtistMusic.setText(artist);
        textViewTimeMusic.setText(milliSecondsToTimer(time));
    }

    private void initToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_down_play_music);
    }

    private void anhXa() {
        toolbar = findViewById(R.id.toolBarPlayMusic);
        imageViewImageMusic = findViewById(R.id.imageViewImageMusic);
        textViewTitleMusic = findViewById(R.id.textViewTitleMusic);
        textViewArtistMusic = findViewById(R.id.textViewArtistMusic);
        seekbarPlayMusic = findViewById(R.id.seebarPlayMusic);
        textViewCurrentTime = findViewById(R.id.textViewCurrentTime);
        textViewTimeMusic = findViewById(R.id.textViewTimeMusic);
        imageViewPlayMusic = findViewById(R.id.imageViewPlayMusic);
        imageViewRepeatMusic = findViewById(R.id.imageViewRepeatMusic);
        imageViewPrevMusic = findViewById(R.id.imageViewPrevMusic);
        imageViewNextMusic = findViewById(R.id.imageViewNextMusic);
        imageViewRandomMusic = findViewById(R.id.imageViewRandomMusic);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_diagram:
                startActivity(new Intent(PlayMusicActivity.this, SettingVolumeActivity.class));
                break;
            case R.id.menu_share:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_play_music, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private String milliSecondsToTimer(String duration) {
        String finalTimerString = "";
        String secondsString;
        int hours = (int) (Long.parseLong(duration) / (1000 * 60 * 60));
        int minutes = (int) (Long.parseLong(duration) % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((Long.parseLong(duration) % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        finalTimerString = finalTimerString + minutes + ":" + secondsString;
        return finalTimerString;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewPlayMusic:
                changePlayOrPauseMusic();
                break;
            case R.id.imageViewRepeatMusic:
                changeRepeatMusic();
                break;
            case R.id.imageViewPrevMusic:
                prevMusic();
                break;
            case R.id.imageViewNextMusic:
                nextMusic();
                break;
            case R.id.imageViewRandomMusic:
                changeRandomMusic();
                break;
        }
    }

    private void changeRandomMusic() {
        this.countRandom++;
        switch (countRandom) {
            case 1:
                Toast.makeText(this, "Bật ngẫu nhiên bài hát", Toast.LENGTH_SHORT).show();
                randomMusic();
                sendBroadcastService(7, false, false);
                imageViewRandomMusic.setImageResource(R.drawable.ic_shuffle_red);
                break;
            case 2:
                Toast.makeText(this, "Tắt ngẫu nhiên bài hát", Toast.LENGTH_SHORT).show();
                unableRandomMusic();
                sendBroadcastService(8, false, false);
                imageViewRandomMusic.setImageResource(R.drawable.ic_random_play_musisc);
                this.countRandom = 0;
                break;
        }
    }

    private void unableRandomMusic() {
        this.positionRandom = this.position;
    }

    private void randomMusic() {
        this.positionRandom = new Random().nextInt(((this.listAudioModels.getAudioModelList().size() - 1) - 0 + 1)) + 0;
    }

    private void repeatMusic() {
        this.path = this.listAudioModels.getAudioModelList().get(this.position).getaPath();
        this.title = this.listAudioModels.getAudioModelList().get(this.position).getaName();
        this.artist = this.listAudioModels.getAudioModelList().get(this.position).getaArtist();

        changeDataView(this.path, this.title, this.artist);
        initValue();

        this.secondSeekBar = 0;
        seekbarPlayMusic.setProgress(this.secondSeekBar);
        seekbarPlayMusic.setMax(this.secondTotal);
        this.isCheckedPause = false;
        this.imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
    }

    private void nextMusic() {
        if (this.position == this.listAudioModels.getAudioModelList().size() - 1) {
            this.position = 0;
        } else this.position++;
        changeMusicNextOrPrev();
    }

    private void changeMusicNextOrPrev() {
        this.path = this.listAudioModels.getAudioModelList().get(this.position).getaPath();
        this.title = this.listAudioModels.getAudioModelList().get(this.position).getaName();
        this.artist = this.listAudioModels.getAudioModelList().get(this.position).getaArtist();

        changeDataView(this.path, this.title, this.artist);
        initValue();

        this.secondSeekBar = 0;
        seekbarPlayMusic.setProgress(this.secondSeekBar);
        seekbarPlayMusic.setMax(this.secondTotal);
        this.isCheckedPause = false;
        this.imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);

        if (isDelete) {
            Intent serviceIntent = new Intent(this, NotificationService.class);
            serviceIntent.putExtra("listAudio", this.listAudioModels);
            serviceIntent.putExtra("position", position);
            serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            startService(serviceIntent);
            isDelete = false;
        } else {
            sendBroadcastService(3, this.isChangeSeekBar, !isChangeMusic);
        }
    }

    private void changeDataView(String path, String title, String artist) {
        imageViewImageMusic.setImageBitmap(Constants.getImageMusic(path));
        textViewTitleMusic.setText(title);
        textViewArtistMusic.setText(artist);
        time = this.listAudioModels.getAudioModelList().get(this.position).getDuration();
        textViewTimeMusic.setText(milliSecondsToTimer(time));
    }

    private void prevMusic() {
        if (this.position == 0) {
            this.position = this.listAudioModels.getAudioModelList().size() - 1;
        } else this.position--;

        changeMusicNextOrPrev();
    }

    private void changeRepeatMusic() {
        this.countRepeat++;
        switch (this.countRepeat) {
            case 1:
                Toast.makeText(this, "Lặp lại tất cả", Toast.LENGTH_SHORT).show();
                imageViewRepeatMusic.setImageResource(R.drawable.ic_repeat_all_play_music);
                sendBroadcastService(4, false, false);
                break;
            case 2:
                Toast.makeText(this, "Lặp lại một lần", Toast.LENGTH_SHORT).show();
                imageViewRepeatMusic.setImageResource(R.drawable.ic_repeat_one_play_music);
                sendBroadcastService(5, false, false);
                break;
            case 3:
                Toast.makeText(this, "Tắt lặp lại", Toast.LENGTH_SHORT).show();
                imageViewRepeatMusic.setImageResource(R.drawable.ic_repeat_black_24dp);
                sendBroadcastService(6, false, false);
                this.countRepeat = 0;
                break;
        }
    }

    private void changePlayOrPauseMusic() {
        isCheckedPause = !isCheckedPause;
        if (isCheckedPause) {
            imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
            sendBroadcastService(2, isChangeSeekBar, false);
        } else {
            if (isDelete) {
                Intent serviceIntent = new Intent(this, NotificationService.class);
                serviceIntent.putExtra("listAudio", this.listAudioModels);
                serviceIntent.putExtra("position", position);
                serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                startService(serviceIntent);
                isDelete = false;
            } else {
                sendBroadcastService(1, isChangeSeekBar, false);
            }
            imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int i = intent.getIntExtra("key", 0);
            switch (i) {
                case 1:
                    isCheckedPause = true;
                    isDelete = true;
                    imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
                    break;
                case 0:
                    isCheckedPause = intent.getBooleanExtra("isPlay", false);
                    if (isCheckedPause) {
                        imageViewPlayMusic.setImageResource(R.drawable.ic_play_arrow_black_big);
                    } else imageViewPlayMusic.setImageResource(R.drawable.ic_pause_black_big);
                    break;
                case 2:
                    position = intent.getIntExtra("position", 0);
                    changeMusic();
                    break;
            }
        }
    };

    private void changeMusic() {
        this.path = listAudioModels.getAudioModelList().get(position).getaPath();
        this.title = listAudioModels.getAudioModelList().get(position).getaName();
        this.artist = listAudioModels.getAudioModelList().get(position).getaArtist();
        this.time = listAudioModels.getAudioModelList().get(position).getDuration();

        imageViewImageMusic.setImageBitmap(Constants.getImageMusic(this.path));
        textViewTitleMusic.setText(this.title);
        textViewArtistMusic.setText(this.artist);
        textViewTimeMusic.setText(milliSecondsToTimer(this.time));

        initValue();

        secondSeekBar = 0;
        seekbarPlayMusic.setMax(secondTotal);
    }

    @Override
    protected void onStart() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter("SOS"));
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }


}
